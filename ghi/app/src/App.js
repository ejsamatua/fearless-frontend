import React from "react";
import {Routes, Route, BrowserRouter,} from 'react-router-dom'
import Nav from "./Nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendConferenceForm from "./AttendConferenceForm";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <React.Fragment>
      <BrowserRouter>
          <Nav />
          <div className="container">

            <Routes>
            {/* route for home page */}
              <Route index element={<MainPage />}>

              </Route>

            {/* route for location */}
              <Route path="locations">
                <Route path="new" element={<LocationForm />} />
              </Route>

            {/* route for attendees list */}
              <Route path="attendees" >
                <Route path="" element={<AttendeesList attendees={props.attendees} />} />
                <Route path="new" element={<AttendConferenceForm />} />
              </Route>

            {/* route for conferences */}
              <Route path="conferences">
                <Route path="new" element={<ConferenceForm />} />
              </Route>


            {/* route for presentation */}
              <Route path="presentations">
                <Route path="new" element={<PresentationForm />} />
              </Route>

            </Routes>
          </div>
        </BrowserRouter>
    </React.Fragment>
  );
}

export default App;
