import React, {useEffect, useState} from 'react';

function ConferenceForm() {

//this is handling the submit of the form

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = presentations;
        data.max_attendees = attendees;
        data.location = location;

        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);
        }
    }

// sets the values of the states depending on the value in our respective inputs

  const [name, setName] = useState('');
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const [starts, setStarts] = useState('');
  const handleStartsChange = (event) => {
    const value = event.target.value;
    setStarts(value);
  }

  const [ends, setEnds] = useState('');
  const handleEndsChange = (event) => {
    const value = event.target.value;
    setEnds(value);
  }

  const [description, setDescription] = useState('');
  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }

  const [presentations, setPresentations] = useState('');
  const handlePresentationsChange = (event) => {
    const value = event.target.value;
    setPresentations(value);
  }

  const [attendees, setAttendees] = useState('');
  const handleAttendeesChange = (event) => {
    const value = event.target.value;
    setAttendees(value);
  }

  const [location, setLocation] = useState('');
  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

//this gets our states to show up on the drop down menu

  const [locations, setLocations] = useState([]);
  const fetchData = async () => {

    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations)
    }
  }
  useEffect(() => {
    fetchData();
  }, []);

// our jsx

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new conference</h1>
        <form onSubmit={handleSubmit} id="create-conference-form">
          <div className="form-floating mb-3">
            <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
            <label htmlFor="name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleStartsChange} placeholder="Start Date" required type="date" name="starts" id="starts" className="form-control" />
            <label htmlFor="starts">Start Date</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleEndsChange} placeholder="End Date" required type="date" name="ends" id="ends" className="form-control" />
            <label htmlFor="ends">End Date</label>
          </div>

          <div className="mb-3">
            <textarea onChange={handleDescriptionChange} required placeholder="Description" name="description" id="description" className="form-control" cols="30" rows="10"></textarea>
          </div>

          <div className="form-floating mb-3">
            <input onChange={handlePresentationsChange} placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
            <label htmlFor="max_presentations">Max Presentations</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleAttendeesChange} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
            <label htmlFor="max_attendees">Max Attendees</label>
          </div>
          <div className="mb-3">
            <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                    return (
                        <option key={location.name} value={location.id}>
                        {location.name}
                        </option>
                    );
                })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  );
}

export default ConferenceForm
